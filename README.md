# Raspberry Pi FM receiver using Python 3 I2C and Tea5767 #

**Contributor:** Ahmed GHANMI

**Description:![radio.PNG](https://bitbucket.org/repo/q84k8j/images/1219100035-radio.PNG)**
This is a simple TEA5767 driver to tune into a local radio station
with Raspberry Pi 2. You can either use the console program or run the web interface.

<img src="tea5767.png" />

**Running on Linux command console:
** 

sudo python3 tea5767controller.py

**Running the Web Interface
** 

*Run it with:* 

sudo python3 tea5767_tornado_server.py

Open browser from a client computer: http://IPADDRESSOFYOURPI:8888


*Example:* http:/192.168.137.104:8888